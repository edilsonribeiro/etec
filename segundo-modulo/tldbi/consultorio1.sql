CREATE DATABASE consultorio;

 /*Criar uma base de dados*/ /*mostrar as bases de dados existentes*/ SHOW databases;

 /*usar uma base de dados*/ USE consultorio;

 /*Criar tabelas
	create table pessoa(pCpf int primary key, pEmail varchar(50), pDataNasc date, pTelefone);*/
CREATE TABLE paciente(pacCodigo int PRIMARY KEY, pacNome varchar(40), pacFone char(13), pacCelular char(14), pacConvenio varchar(30));


CREATE TABLE medico(medCrm int PRIMARY KEY, medNome varchar(40), medFone char(13), medEspecialidade varchar(30));

 /*exibir detalhes de uma tabela*/ DESC medico;

 DESC paciente;