<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Resultado Cadastro</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>
	<div class="container-fluid">
		<div id="cabecalho" class="row">
			<h1>Escola Nova Art</h1>
		</div>
		<div id="menu" class="row">
			<ul class="nav nav-pills">
				<li role="presentation" class="active"><a href="index.html">CADASTRO</a></li>
				<li role="presentation"><a href="boletim.html">BOLETIM</a></li>
			</ul>
		</div>
		<div id="corpo" class="row">
			<h2>Cadastro de Alunos</h2>
			<?php 
			$matricula = $_GET["matricula"];
			$nome = $_GET["nome"];
			$sala = $_GET["sala"];
			$periodo = $_GET["periodo"];
			$atividade = $_GET["futebol"] ." ". $_GET["ingles"] ." ". $_GET["teatro"];

			echo "Matricula: $matricula </br>";
			echo "Nome: $nome </br>";
			echo "Sala: $sala </br>";
			echo "Período: $periodo </br>";
			echo "Atividade Extra: $atividade </br>";
			?>
		</div>
		<div id="rodape" class="row">
			<h6>Av. São José, 1010 - Ourinhos-SP - CEP: 19940-000 - Fone: (14) 3322-2001 </h6>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="recursos/js/bootstrap.min.js"></script>
</body>
</html>