<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>
	<div class="container-fluid">
		<div id="cabecalho" class="row">
			<h1>Escola Nova Art</h1>
		</div>
		<div id="menu" class="row">
			<ul class="nav nav-pills">
				<li role="presentation"><a href="index.html">CADASTRO</a></li>
				<li role="presentation" class="active"><a href="boletim.html">BOLETIM</a></li>
			</ul>
		</div>
		<div id="corpo" class="row">
			<?php 
			$situacao = 0;

			$m1 = $_GET["m1"];
			$m2 = $_GET["m2"];
			$m3 = $_GET["m3"];
			$m4 = $_GET["m4"];

			$p1 = $_GET["p1"];
			$p2 = $_GET["p2"];
			$p3 = $_GET["p3"];
			$p4 = $_GET["p4"];

			$h1 = $_GET["h1"];
			$h2 = $_GET["h2"];
			$h3 = $_GET["h3"];
			$h4 = $_GET["h4"];

			$g1 = $_GET["g1"];
			$g2 = $_GET["g2"];
			$g3 = $_GET["g3"];
			$g4 = $_GET["g4"];

			$c1 = $_GET["c1"];
			$c2 = $_GET["c2"];
			$c3 = $_GET["c3"];
			$c4 = $_GET["c4"];

			$a1 = $_GET["a1"];
			$a2 = $_GET["a2"];
			$a3 = $_GET["a3"];
			$a4 = $_GET["a4"];

			$e1 = $_GET["e1"];
			$e2 = $_GET["e2"];
			$e3 = $_GET["e3"];
			$e4 = $_GET["e4"];
			?>
			<div class="table-responsive">
				<table width="70%"  summary="Boletim - Escola Nova Art" class="table table-hover table-condensed table-striped ">
					<caption>Boletim</caption>
					<thead>
						<tr>
							<td>Matéria</td>
							<td>1º Bimestre</td>
							<td>2º Bimestre</td>
							<td>3º Bimestre</td>
							<td>4º Bimestre</td>
							<td>Média</td>
							<td>Situação</td>
						</tr>
					</thead>
					<tbody>
						<?php  $media = (($m1+$m2+$m3+$m4)/4);
						$mensagem = " ";
						?>
						<tr <?php 
						if ($media >= 7) {
							$mensagem = "APROVADO";
							echo "class=\"success\"";
						}else{
							$mensagem = "REPROVADO";
							echo "class=\"danger\"";
						} ?>
						>
						<td>Matemática</td>
						<td><?php 						echo $m1;						?></td>
						<td><?php 						echo $m2;						?></td>
						<td><?php 						echo $m3;						?></td>
						<td><?php 						echo $m4;						?></td>
						<td><?php echo $media; ?></td>
						<td><?php echo $mensagem; ?></td>
					</tr>
						<?php  $media = (($p1+$p2+$p3+$p4)/4);
						$mensagem = " ";
						?>
						<tr <?php 
						if ($media >= 7) {
							$mensagem = "APROVADO";
							echo "class=\"success\"";
						}else{
							$mensagem = "REPROVADO";
							echo "class=\"danger\"";
							$situacao = 1;
						} ?>
						>
						<td>Português</td>
						<td><?php 						echo $p1;						?></td>
						<td><?php 						echo $p2;						?></td>
						<td><?php 						echo $p3;						?></td>
						<td><?php 						echo $p4;						?></td>
						<td><?php echo $media; ?></td>
						<td><?php echo $mensagem; ?></td>
					</tr>
						<?php  $media = (($h1+$h2+$h3+$h4)/4);
						$mensagem = " ";
						?>
						<tr <?php 
						if ($media >= 7) {
							$mensagem = "APROVADO";
							echo "class=\"success\"";
						}else{
							$mensagem = "REPROVADO";
							echo "class=\"danger\"";
							$situacao = 1;
						} ?>
						>
						<td>História</td>
						<td><?php 						echo $h1;						?></td>
						<td><?php 						echo $h2;						?></td>
						<td><?php 						echo $h3;						?></td>
						<td><?php 						echo $h4;						?></td>
						<td><?php echo $media; ?></td>
						<td><?php echo $mensagem; ?></td>
					</tr>
						<?php  $media = (($g1+$g2+$g3+$g4)/4);
						$mensagem = " ";
						?>
						<tr <?php 
						if ($media >= 7) {
							$mensagem = "APROVADO";
							echo "class=\"success\"";
						}else{
							$mensagem = "REPROVADO";
							echo "class=\"danger\"";
							$situacao = 1;
						} ?>
						>
						<td>Geografia</td>
						<td><?php 						echo $g1;						?></td>
						<td><?php 						echo $g2;						?></td>
						<td><?php 						echo $g3;						?></td>
						<td><?php 						echo $g4;						?></td>
						<td><?php echo $media; ?></td>
						<td><?php echo $mensagem; ?></td>
					</tr>
						<?php  $media = (($c1+$c2+$c3+$c4)/4);
						$mensagem = " ";
						?>
						<tr <?php 
						if ($media >= 7) {
							$mensagem = "APROVADO";
							echo "class=\"success\"";
						}else{
							$mensagem = "REPROVADO";
							echo "class=\"danger\"";
							$situacao = 1;
						} ?>
						>
						<td>Ciência</td>
						<td><?php 						echo $c1;						?></td>
						<td><?php 						echo $c2;						?></td>
						<td><?php 						echo $c3;						?></td>
						<td><?php 						echo $c4;						?></td>
						<td><?php echo $media; ?></td>
						<td><?php echo $mensagem; ?></td>
					</tr>
						<?php  $media = (($a1+$a2+$a3+$a4)/4);
						$mensagem = " ";
						?>
						<tr <?php 
						if ($media >= 7) {
							$mensagem = "APROVADO";
							echo "class=\"success\"";
						}else{
							$mensagem = "REPROVADO";
							echo "class=\"danger\"";
							$situacao = 1;
						} ?>
						>
						<td>Artes</td>
						<td><?php 						echo $a1;						?></td>
						<td><?php 						echo $a2;						?></td>
						<td><?php 						echo $a3;						?></td>
						<td><?php 						echo $a4;						?></td>
						<td><?php echo $media; ?></td>
						<td><?php echo $mensagem; ?></td>
					</tr>
						<?php  $media = (($e1+$e2+$e3+$e4)/4);
						$mensagem = " ";
						?>
						<tr <?php 
						if ($media >= 7) {
							$mensagem = "APROVADO";
							echo "class=\"success\"";
						}else{
							$mensagem = "REPROVADO";
							echo "class=\"danger\"";
							$situacao = 1;
						} ?>
						>
						<td>Educação Física</td>
						<td><?php 						echo $e1;						?></td>
						<td><?php 						echo $e2;						?></td>
						<td><?php 						echo $e3;						?></td>
						<td><?php 						echo $e4;						?></td>
						<td><?php echo $media; ?></td>
						<td><?php echo $mensagem; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<?php 
			if ($situacao == 0) {
				echo "<h2 class=\"text-success\">APROVADO<\h1>";
			}else{
				echo "<h2 class=\"text-danger\">REPROVADO</h1>";
			} 

		 ?>
	</div>
	<div id="rodape" class="row">
		<h6>Av. São José, 1010 - Ourinhos-SP - CEP: 19940-000 - Fone: (14) 3322-2001 </h6>
	</div>

</div> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="recursos/js/bootstrap.min.js"></script>
</body>
</html>